#+title: Talk Like Ted

* Do
- What makes my heart sing?
- Accept happiness as a choice
- Invite passionate people into your life
- Collect and tell personal stories
- When you record your presentation walk out of the frame once in a while
- Fake it till you become it
- Reveal information that's completely new to your audience
- Demonstrate the big picture before details -- easier to understand
- Learn about humor
- Deliver presentations which touches more then one sense
* Know
- People need to be inspired themselves before they can inspire others
- Stories are just data with a soul
- Attention -> Break a pattern!
- Curiosity happens when we feel a gap in our knowledge
- 90% of all businesses succeed the second time
- Every no means you are one step closer to a yes
- Ideas are the currency of today, stories facilitate the exchange
- Powerposing increases testosterone and lowers cortisol
- Endorphins counteract fear
- People can remember three pieces of information very well
- Repetition frees your mind to tell your story
